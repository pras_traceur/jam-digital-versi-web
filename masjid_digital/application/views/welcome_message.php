<!doctype html>
<html lang="en">
  <head>
    <title>Masjid Digital</title>
    <link rel="icon" href="<?php echo base_url();?>assets/image//icon.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"></link>

    <link href="<?php echo base_url();?>assets/style.css" rel="stylesheet">
  </head>
  <body scroll="no" style="overflow: hidden" onload="startTime()" id="full">
    <div class="bg">
          <div class="row"><br><br></div>
          <div class="row">

            <div class="col-sm-8">
              <div class="row">
                <span class="judul" style="padding-left: 40px" id="user_nama"></span>
              </div>
              <div class="row">
                <span class="alamat" style="padding-left: 40px" id="user_alamat"></span>
              </div>
            </div>
            <div class="col-sm-4">
            <div class="row">
              <div class="col-sm-10">
                <div class="row">
                 <div class="col-sm" style="text-align: right;">
                  <button class="waktu btn btn-danger"><span id="jam"></span></button>&nbsp;
                  <button class="waktu btn btn-danger"><span id="menit"></span></button>&nbsp;
                  <button class="waktu btn btn-danger"><span id="detik"></span></button>
                </div>
                </div>
                <div class="row">
                  <div class="col-sm" style="text-align: right;">
                    <h1 style="color:white;font-weight: bold;font-size: 210%;"><span id="hari"></span></h1>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm" style="text-align: right;">
                    <h1 style="color:crimson;font-weight: bold;font-size: 110%;text-shadow: 1px 1px 1px rgba(255, 255, 255, 0.8);"><span id="tanggal"></span></h1>
                  </div>
                </div>
              </div>
              <div class="col-sm-2"></div>
            </div>
          </div>
          </div>
         <br><br><br><br> 
          <div class="jumbotron jumbotron-fluid">
              <div class="container">
                <h1 class="display-4" id="konten_atas"></h1>
                <p class="lead" id="konten_tengah"></p>
              </div>
          </div>
	<br><br>
          <div class="container">
          <div class="row">
              <div class="sholat col-sm-2">
                  <button type="button" id="satu" class="btn btn-outline-danger">
                    Imsak
                    <p style="font-size: 50px;color: white;font-weight: bold;" id="imsak"></p>
                  </button>
              </div>
              <div class="sholat col-sm-2">
                  <button type="button" id="dua" class="btn btn-outline-danger">
                    Subuh
                    <p style="font-size: 50px;color: white;font-weight: bold;" id="subuh"></p>
                  </button>
              </div>
              <div class="sholat col-sm-2">
                  <button type="button" id="tiga" class="btn btn-outline-danger">
                    <div id="jumat">Dzuhur</div>
                    <p style="font-size: 50px;color: white;font-weight: bold;" id="dzuhur"></p>
                  </button>
              </div>
              <div class="sholat col-sm-2">
                  <button type="button" id="empat" class="btn btn-outline-danger">
                    Ashar
                    <p style="font-size: 50px;color: white;font-weight: bold;" id="ashar"></p>
                  </button>
              </div>
              <div class="sholat col-sm-2">
                  <button type="button" id="lima" class="btn btn-outline-danger">
                    Maghrib
                    <p style="font-size: 50px;color: white;font-weight: bold;" id="maghrib"></p>
                  </button>
              </div>
              <div class="sholat col-sm-2">
                  <button type="button" id="enam" class="btn btn-outline-danger">
                    Isya'
                    <p style="font-size: 50px;color: white;font-weight: bold;" id="isya"></p>
                  </button>
              </div> 
          </div>

        </div>

      <footer>
          <br><br><br>
          <div class="footer text-center">
            <marquee id="konten_bawah">
              
            </marquee>
          </div>
      </footer>
    </div>

    <script src="<?php echo base_url();?>assets/js/jquery-slim.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    
    <script>
        function startTime() {
          var today = new Date();
          var h = today.getHours();
          var m = today.getMinutes();
          var s = today.getSeconds();
          m = checkTime(m);
          s = checkTime(s);
          if(h<10){
            document.getElementById('jam').innerHTML = '0'+h;
          }else{
            document.getElementById('jam').innerHTML = h;
          }
            document.getElementById('menit').innerHTML = m;
            document.getElementById('detik').innerHTML = s;
        
          var t = setTimeout(startTime, 500);
        }
        function checkTime(i) {
          if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
          return i;
        }
        </script>
        <script language="JavaScript">
            var tanggallengkap = new String();
            var namahari = ("Minggu Senin Selasa Rabu Kamis Jumat Sabtu");
            namahari = namahari.split(" ");
            var namabulan = ("Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember");
            namabulan = namabulan.split(" ");
            var tgl = new Date();
            var hari = tgl.getDay();
            var tanggal = tgl.getDate();
            var bulan = tgl.getMonth();
            var tahun = tgl.getFullYear();
            tanggallengkap = tanggal + " " + namabulan[bulan] + " " + tahun;
            document.getElementById('hari').innerHTML = namahari[hari];
            document.getElementById('tanggal').innerHTML = tanggallengkap;
        </script>

        <script>
          var elem = document.getElementById("full");
          if (elem.requestFullscreen) {
          elem.requestFullscreen();
          } else if (elem.msRequestFullscreen) {
          elem.msRequestFullscreen();
          } else if (elem.webkitRequestFullscreen) {
          elem.webkitRequestFullscreen();
          }
        </script>

        <script> 
        var subuh = "", dzuhur = "", ashar = "", maghrib = "", isya = "";
        var i = 1;var now = "";
        function clear_button(){
          document.getElementById("dua").className = "btn btn-outline-danger";
          document.getElementById("tiga").className = "btn btn-outline-danger";
          document.getElementById("empat").className = "btn btn-outline-danger";
          document.getElementById("lima").className = "btn btn-outline-danger";
          document.getElementById("enam").className = "btn btn-outline-danger";
        }
          setInterval(function(){
            
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            if(h<10){
              h = '0'+h;
            }
            if(m<10){
              m = '0'+m;
            }
            now = h+':'+m;
            if(now > subuh && now < '05:00'){
              if(i%2 != 0){
                document.getElementById("dua").className = "btn btn-danger";
              }else{
                clear_button();
              }
            }else if(now > dzuhur && now < ashar){
              if(i%2 != 0){
                document.getElementById("tiga").className = "btn btn-danger";
              }else{
                clear_button();
              }
            }else if(now > ashar && now < maghrib){
              if(i%2 != 0){
                document.getElementById("empat").className = "btn btn-danger";
              }else{
                clear_button();
              }
            }else if(now > maghrib && now < isya){
              if(i%2 != 0){
                document.getElementById("lima").className = "btn btn-danger";
              }else{
                clear_button();
              }
            }else if(now < dzuhur && now > '05:00'){
                clear_button();
            }
            else{
              if(i%2 != 0){
                document.getElementById("enam").className = "btn btn-danger";
              }else{
                clear_button();
              }
            }
            i++;
            
          },2000);
        </script>


        <!-- Looping -->
        <script> 
            function get_user_nama(){
              $.ajax({
                  type: 'GET',
                  url: '<?php echo base_url();?>index.php/Welcome/get_user_loop/user_nama',
                  success: function (html) {
                      $("#user_nama").html(html);
                  }
              });
            }

            function get_user_alamat(){
              $.ajax({
                  type: 'GET',
                  url: '<?php echo base_url();?>index.php/Welcome/get_user_loop/user_alamat',
                  success: function (html) {
                      $("#user_alamat").html(html);
                  }
              });
            }

            function get_konten_atas(){
              $.ajax({
                  type: 'GET',
                  url: '<?php echo base_url();?>index.php/Welcome/get_konten_atas_loop/konten_atas_isi',
                  success: function (html) {
                      $("#konten_atas").html(html);
                  }
              });
            }

            function get_konten_tengah(){
              $.ajax({
                  type: 'GET',
                  url: '<?php echo base_url();?>index.php/Welcome/get_konten_tengah_loop/konten_tengah_isi',
                  success: function (html) {
                      $("#konten_tengah").html(html);
                  }
              });
            }

            function get_konten_bawah(){
              $.ajax({
                  type: 'GET',
                  url: '<?php echo base_url();?>index.php/Welcome/get_konten_bawah_loop',
                  success: function (html) {
                      $("#konten_bawah").html(html);
                  }
              });
            }

            function get_sholat(){
              $.ajax({
                  type: 'GET',
                  url: '<?php echo base_url();?>index.php/Welcome/get_sholat_loop',
                  success: function (data) {
                      var objData = jQuery.parseJSON(data);
                      $('#imsak').html(objData[0].sholat_jam);
                      $('#subuh').html(objData[0].sholat_jam);
                      $('#dzuhur').html(objData[1].sholat_jam);
                      $('#ashar').html(objData[2].sholat_jam);
                      $('#maghrib').html(objData[3].sholat_jam);
                      $('#isya').html(objData[4].sholat_jam);
                      
                      subuh = objData[0].sholat_jam;
                      dzuhur = objData[1].sholat_jam;
                      ashar = objData[2].sholat_jam;
                      maghrib = objData[3].sholat_jam;
                      isya = objData[4].sholat_jam;
                  }
              });
            }

            setInterval(function(){
              //jika hari jumat
              var d = new Date();
              var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
              if(days[d.getDay()] == "Friday"){
                $("#jumat").html("Jum'at");
              }else{
                $("#jumat").html("Dzuhur");
              }

              //jalankan ajax
              get_user_nama();
              get_user_alamat();
              get_konten_atas();
              get_konten_tengah();
              get_sholat();
              get_konten_bawah();

              //jika waktu sholat
              if(now == subuh && now != ""){
                window.location.replace('<?php echo base_url();?>index.php/Countdown/get_sholat/1');
              }else if(now == dzuhur && now != ""){
                if(days[d.getDay()] == "Friday"){
                  window.location.replace('<?php echo base_url();?>index.php/Jumat/get_sholat/2');
                }else{
                  window.location.replace('<?php echo base_url();?>index.php/Countdown/get_sholat/2');
                }
              }else if(now == ashar && now != ""){
                window.location.replace('<?php echo base_url();?>index.php/Countdown/get_sholat/3');
              }else if(now == maghrib && now != ""){
                window.location.replace('<?php echo base_url();?>index.php/Countdown/get_sholat/4');
              }else if(now == isya && now != ""){
                window.location.replace('<?php echo base_url();?>index.php/Countdown/get_sholat/5');
              }

            },1000);
        </script>
  </body>
</html>
