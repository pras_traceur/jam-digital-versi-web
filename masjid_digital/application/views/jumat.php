<!doctype html>
<html lang="en">
  <head>
    <title>Masjid Digital</title>
    <link rel="icon" href="<?php echo base_url();?>assets/image//icon.png">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet"></link>

    
    <style type="text/css">
    	#first {
		    z-index: 2;
		}

		#second {
		    position: fixed;
		    width: 100%;
  			overflow: hidden;
		    z-index: 0;
		}
    </style>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/load/normalize.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/load/style.css">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700%7CPoppins:400,500" rel="stylesheet">
	
	
	<link href="<?php echo base_url();?>assets/load/common-css/ionicons.css" rel="stylesheet">
	
	
	<link rel="<?php echo base_url();?>assets/load/stylesheet" href="common-css/jquery.classycountdown.css" />
		
	<link href="<?php echo base_url();?>assets/load/07-comming-soon/css/styles.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>assets/load/07-comming-soon/css/responsive.css" rel="stylesheet">
  </head>
  <body>
  <audio autoplay>
  	<source src="<?php echo base_url();?>assets/audio/beep.mp3" type="audio/mpeg">
  </audio>
  
  	<div class="main-area center-text" style="background-image:url(<?php echo base_url();?>assets/load/images/countdown-6-1600x900.jpg);" id="second">
			
			<div class="display-table" id="adzan">
				<div class="display-table-cell">
					
					<center>
						<img src="<?php echo base_url();?>assets/load/images/warning.png" style="width: 20%">
					</center>
					<h1 class="title"><b>Waktu Adzan Sholat Jum'at</b></h1>
					<p class="desc font-white">Harap Tenang saat Adzan Berkumandang</p>
					<br>
					
				</div><!-- display-table -->
			</div><!-- display-table-cell -->

			<div class="display-table" id="iqomah">
				<div class="display-table-cell">
					
					<h1 class="title"><b>IQOMAH</b></h1>
					<p class="desc font-white">Menjelang Sholat Jum'at Berjamaah</p>
					<br>
					<center>
						<img src="<?php echo base_url();?>assets/load/images/larangan.png" style="width: 40%">
					</center>
					<br>
					<?php
						$endTime = strtotime("+30 minutes", strtotime($sholat['sholat_jam']));
						$jam_sholat = date("Y-m-d").' '.date('H:i', $endTime);
						$jam_adzan = date('H:i', strtotime("+3 minutes", strtotime($sholat['sholat_jam'])));
						$jam_sholat_js_in = date('H:i', $endTime);
						$jam_sholat_js_exit = date('H:i', strtotime("+2 minutes", strtotime($jam_sholat_js_in)));
					?>			
					<div id="normal-countdown" data-date="<?php echo $jam_sholat;?>"></div>
					
				</div><!-- display-table -->
			</div><!-- display-table-cell -->

			<div class="display-table" id="waktu_sholat">
				<div class="display-table-cell">

					<center>
						<img src="<?php echo base_url();?>assets/load/images/matikan.png" style="width: 40%">
					<br><br>
					<h1 class="title" style="font-size: 50px;"><b>Harap Tenang saat Khutbah berlangsung</b></h1>
							
					</center>

				</div><!-- display-table -->
			</div><!-- display-table-cell -->

		</div><!-- main-area -->

		<!-- SCIPTS -->
	
		<script src="<?php echo base_url();?>assets/load/common-js/jquery-3.1.1.min.js"></script>
		
		<script src="<?php echo base_url();?>assets/load/common-js/jquery.countdown.min.js"></script>
		
		<script src="<?php echo base_url();?>assets/load/common-js/scripts.js"></script>


		<div class="container" id="first">
		  <div class="left-layer"></div>
		  <div class="left-layer left-layer--2"></div>
		  <div class="left-layer left-layer--3"></div>
		  <div class="left-layer left-layer--4"></div>
		  <div class="right-layer"></div>
		  <div class="right-layer right-layer--2"></div>
		  <div class="right-layer right-layer--3"></div>
		</div>
  
    <script type="text/javascript">
    	
    	window.onload = function() {
		     animasi();
		     
		    $('#adzan').show();
		    $('#iqomah').hide();
		    $('#waktu_sholat').hide();
    	};

    	function animasi(){
    		var id = 'right';
		    var layerClass = "." + id+ "-layer";
		    var layers = document.querySelectorAll(layerClass);
		    for (const layer of layers) {
		      layer.classList.toggle("active");
		    }
    	}

    	var j = 2;
    	var waktu_adzan = "<?php echo $jam_adzan;?>";
    	var waktu_sholat = "<?php echo $jam_sholat_js_in;?>";
    	var waktu_sholat_selesai = "<?php echo $jam_sholat_js_exit;?>";

    	setInterval(function(){
    		var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            if(h<10){
              h = '0'+h;
            }
            if(m<10){
              m = '0'+m;
            }
            now = h+':'+m;

            if(now == waktu_adzan){
            	if(j==1){
	    			animasi();
	    			$('#adzan').hide();
				    $('#iqomah').show();
				    $('#waktu_sholat').hide();
	    			j++;j++;
    			}
    		}
            if(now == waktu_sholat){
            	if(j == 2){
	            	animasi();
	            	$('#adzan').hide();
			    	$('#iqomah').hide();
			    	$('#waktu_sholat').show();
			    	j++;
		    	}
            }
            if(now == waktu_sholat_selesai){
            	if(j == 3){
	            	animasi();
	            	window.location.replace('<?php echo base_url();?>');
	            	j++;
	            }
            }

    	},1000);
    </script>
</body>
</html>
