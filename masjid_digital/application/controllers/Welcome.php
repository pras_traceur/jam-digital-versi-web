<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
    	$this->load->database();
		$this->load->helper('url');
    }

	public function index()
	{	
		$this->load->view('welcome_message');
	}

	//user
	public function get_user_loop($field) {
	    $hasil = $this->db->query('Select * from user')->row_array();
	    echo $hasil[$field];
	}

	//konten atas
	public function get_konten_atas_loop($field) {
	    $hasil = $this->db->query('Select * from konten_atas')->row_array();
	    echo $hasil[$field];
	}

	//konten tengah
	public function get_konten_tengah_loop($field) {
	    $hasil = $this->db->query('Select * from konten_tengah')->row_array();
	    echo $hasil[$field];
	}

	//sholat
	public function get_sholat_loop() {
	    $hasil = $this->db->query('Select DATE_FORMAT(sholat_jam, "%H:%i") as `sholat_jam` from sholat')->result_array();
        echo json_encode($hasil);
	}

	//footer
	public function get_konten_bawah_loop() {
	    $hasil = $this->db->query('Select * from konten_bawah')->result_array();
	    foreach ($hasil as $key) {
            echo '<img src="'.base_url().'assets/image/megaphone.png" style="width:2%">&nbsp;'.$key['konten_bawah_isi'].'&nbsp;&nbsp;&nbsp;';
        }      
	}

}
