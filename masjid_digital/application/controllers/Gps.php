<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Gps extends REST_Controller {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->helper('url');
    }

    function index_post() {
        $id = $this->post('id');
    	$data = array(
            'sholat_jam'     => $this->post('sholat_jam')
            );
        $this->db->where('sholat_id', $id);
        $update = $this->db->update('sholat', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
