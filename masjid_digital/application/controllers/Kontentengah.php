<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Kontentengah extends REST_Controller {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->helper('url');
    }

    public function index_get()
    {
        $help = $this->db->get('konten_tengah')->result();
        $this->response($help, 200);
    }

    public function index_post() {
    	$data = array(
            'konten_tengah_isi'     => $this->post('konten_tengah_isi')
            );
        $this->db->where('konten_tengah_id', 1);
        $update = $this->db->update('konten_tengah', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
