<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Sholat extends REST_Controller {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->helper('url');
    }

    public function index_get()
    {
        $help = $this->db->query('Select sholat_id,sholat_waktu,DATE_FORMAT(sholat_jam, "%H:%i") as `sholat_jam` from sholat')->result();
        $this->response($help, 200);
    }

    public function index_post() {
        $id = $this->post('sholat_id');
    	$data = array(
            'sholat_jam'     => $this->post('sholat_jam')
            );
        $this->db->where('sholat_id', $id);
        $update = $this->db->update('sholat', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
