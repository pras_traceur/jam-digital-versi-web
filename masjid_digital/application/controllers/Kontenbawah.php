<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Kontenbawah extends REST_Controller {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->helper('url');
    }

    public function index_get()
    {
        $help = $this->db->get('konten_bawah')->result();
        $this->response($help, 200);
    }

    public function index_post() {
        $id = $this->post('konten_bawah_id');
    	$data = array(
            'konten_bawah_isi'     => $this->post('konten_bawah_isi')
            );
        $this->db->where('konten_bawah_id', $id);
        $update = $this->db->update('konten_bawah', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
