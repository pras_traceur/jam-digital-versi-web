<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Countdown extends CI_Controller {

	public function __construct()
    {
    	parent::__construct();
    	$this->load->database();
		$this->load->helper('url');
    }

	//sholat
	public function get_sholat($sholat) {
	    $hasil = $this->db->query('Select sholat_waktu,DATE_FORMAT(sholat_jam, "%H:%i") as `sholat_jam` from sholat where sholat_id = '.$sholat)->row_array();

	    $data['sholat'] = $hasil;

	    $this->load->view('countdown',$data);
	}
}