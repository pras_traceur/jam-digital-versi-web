<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Kontenatas extends REST_Controller {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->helper('url');
    }

    public function index_get()
    {
        $help = $this->db->get('konten_atas')->result();
        $this->response($help, 200);
    }

    public function index_post() {
    	$data = array(
            'konten_atas_isi'     => $this->post('konten_atas_isi')
            );
        $this->db->where('konten_atas_id', 1);
        $update = $this->db->update('konten_atas', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
