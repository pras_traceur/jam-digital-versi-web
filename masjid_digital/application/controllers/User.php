<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class User extends REST_Controller {

	function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
        $this->load->helper('url');
    }

    public function index_get()
    {
        $help = $this->db->get('user')->result();
        $this->response($help, 200);
    }

    public function index_post() {
    	$data = array(
            'user_nama'     => $this->post('user_nama'),
            'user_email'    => $this->post('user_email'),
            'user_password' => $this->post('user_password'),
            'user_alamat'     => $this->post('user_alamat'),
            'user_telepon'     => $this->post('user_telepon')
            //'user_foto'     => $this->put('user_foto'),
            );
        $this->db->where('user_id', 1);
        $update = $this->db->update('user', $data);
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}
