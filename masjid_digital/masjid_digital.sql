-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Feb 2019 pada 03.02
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `masjid_digital`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `help`
--

CREATE TABLE `help` (
  `help_id` int(11) NOT NULL,
  `help_judul` varchar(1000) NOT NULL,
  `help_isi` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `help`
--

INSERT INTO `help` (`help_id`, `help_judul`, `help_isi`) VALUES
(1, 'Developer', 'Deny Prasetyo-087856850432');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konten_atas`
--

CREATE TABLE `konten_atas` (
  `konten_atas_id` int(11) NOT NULL,
  `konten_atas_isi` varchar(1000) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konten_atas`
--

INSERT INTO `konten_atas` (`konten_atas_id`, `konten_atas_isi`) VALUES
(1, 'بِسْــــــــــــــــــمِ اللهِ الرَّحْمَنِ الرَّحِيْمِ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konten_bawah`
--

CREATE TABLE `konten_bawah` (
  `konten_bawah_id` int(11) NOT NULL,
  `konten_bawah_isi` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konten_bawah`
--

INSERT INTO `konten_bawah` (`konten_bawah_id`, `konten_bawah_isi`) VALUES
(1, 'Kajian Tiap Bulan oleh Ustad Deny Prasetyo'),
(2, 'Mohon Hp dimatikan saat Sholat'),
(3, 'Selamat datang di rumah pak Johan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konten_tengah`
--

CREATE TABLE `konten_tengah` (
  `konten_tengah_id` int(11) NOT NULL,
  `konten_tengah_isi` varchar(1000) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konten_tengah`
--

INSERT INTO `konten_tengah` (`konten_tengah_id`, `konten_tengah_isi`) VALUES
(1, 'Bismillah yang berasal dari bahasa Arab yang memiliki arti ?Dengan menyebut nama Allah?. Bacaan ini yang disebut dengan Tasymiyah dan bagi seorang muslim sangat dianjurkan untuk membacanyasetiap akan memulai sesuatu kegiatan terutama hal-hal baik, sehingga yang dikerjakan diniatkan atas atas nama Allah SWT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `musik`
--

CREATE TABLE `musik` (
  `musik_id` int(11) NOT NULL,
  `musik_nama` varchar(1000) NOT NULL,
  `musik_path` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sholat`
--

CREATE TABLE `sholat` (
  `sholat_id` int(11) NOT NULL,
  `sholat_waktu` varchar(1000) NOT NULL,
  `sholat_jam` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sholat`
--

INSERT INTO `sholat` (`sholat_id`, `sholat_waktu`, `sholat_jam`) VALUES
(1, 'Subuh', '04:23:00'),
(2, 'Dzuhur', '12:15:00'),
(3, 'Ashar', '16:42:00'),
(4, 'Maghrib', '18:24:00'),
(5, 'Isya\'', '19:36:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tema`
--

CREATE TABLE `tema` (
  `team_id` int(11) NOT NULL,
  `tema_tipe` varchar(1000) NOT NULL,
  `tema_path` varchar(1000) NOT NULL,
  `tema_aktif` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tema`
--

INSERT INTO `tema` (`team_id`, `tema_tipe`, `tema_path`, `tema_aktif`) VALUES
(1, 'default', '', 'aktif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_nama` varchar(1000) NOT NULL,
  `user_email` varchar(1000) NOT NULL,
  `user_password` varchar(1000) NOT NULL,
  `user_alamat` varchar(1000) NOT NULL,
  `user_telepon` varchar(20) NOT NULL,
  `user_foto` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`user_id`, `user_nama`, `user_email`, `user_password`, `user_alamat`, `user_telepon`, `user_foto`) VALUES
(1, 'Masjid Bening Guru Semesta', 'pras.rubicteam@gmail.com', '123456', 'Jl. Ampera Raya - Jakarta', '087856850432', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `video_nama` varchar(1000) NOT NULL,
  `video_path` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`help_id`);

--
-- Indeks untuk tabel `konten_atas`
--
ALTER TABLE `konten_atas`
  ADD PRIMARY KEY (`konten_atas_id`);

--
-- Indeks untuk tabel `konten_bawah`
--
ALTER TABLE `konten_bawah`
  ADD PRIMARY KEY (`konten_bawah_id`);

--
-- Indeks untuk tabel `konten_tengah`
--
ALTER TABLE `konten_tengah`
  ADD PRIMARY KEY (`konten_tengah_id`);

--
-- Indeks untuk tabel `musik`
--
ALTER TABLE `musik`
  ADD PRIMARY KEY (`musik_id`);

--
-- Indeks untuk tabel `sholat`
--
ALTER TABLE `sholat`
  ADD PRIMARY KEY (`sholat_id`);

--
-- Indeks untuk tabel `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`team_id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indeks untuk tabel `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `help`
--
ALTER TABLE `help`
  MODIFY `help_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `konten_atas`
--
ALTER TABLE `konten_atas`
  MODIFY `konten_atas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `konten_bawah`
--
ALTER TABLE `konten_bawah`
  MODIFY `konten_bawah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `konten_tengah`
--
ALTER TABLE `konten_tengah`
  MODIFY `konten_tengah_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `musik`
--
ALTER TABLE `musik`
  MODIFY `musik_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sholat`
--
ALTER TABLE `sholat`
  MODIFY `sholat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tema`
--
ALTER TABLE `tema`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
